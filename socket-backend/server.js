const app = require('express')();
const cors = require('cors')
const http = require('http').createServer(app);
const io = new require('socket.io')(http, {
    cors: {
        origin: "http://localhost:4200",
        methods: ["GET", "POST", "PATCH"]
    }
} );

app.use(cors());

app.get('/', (req, res) => res.send('hello!'));

io.on('connection', (socket) => {
    console.log('a user connected');
    socket.on('message', (msg) => {
        console.log(msg);
        io.emit('broadcast',  msg);
    });
});

http.listen(3000, () => {
    console.log('listening on *:3000');
});
